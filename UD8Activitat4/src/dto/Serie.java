package dto;

public class Serie {
	//Hago los valores por y les doy un valor por defecto
	private String titulo;
	private int numeroTemporadas = 3;
	private boolean entregado = false;
	private String genero;
	private String creador;
	//Hago los constructores
	public Serie (String titulo, int numeroTemporadas, String genero, String creador) {
		this.titulo = titulo;
		this.numeroTemporadas = numeroTemporadas;
		this.genero = genero;
		this.creador = creador;

	}
	
	public Serie (String titulo, String creador) {
		this.titulo = titulo;
		this.genero = " ";
		this.creador = creador;

	}
	
	public Serie () {
		this.titulo = " ";
		this.genero = " ";
		this.creador = " ";

	}

	@Override
	public String toString() {
		return "Serie [titulo=" + titulo + ", numeroTemporadas=" + numeroTemporadas + ", entregado=" + entregado
				+ ", genero=" + genero + ", creador=" + creador + "]";
	}
}
